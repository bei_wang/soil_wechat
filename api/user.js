//如果用uni请求
import http from '@/common/uni.interface.js';

//如果是用uview请求,就用下面这句
//const http = uni.$u.http


//获取openid
export function program(params) {
	return http.post('api/mini_program/openid', params)
}

//获取用户信息
export function login(params) {
	return http.post('api/mini_program/login', params)
}

//轮播图
export function getBanner(params) {
	return http.get('api/index/banner', params)
}


//报名
export function add(params) {
	return http.post('api/pay/add', params)
}

//参会身份
export function grouping(params) {
	return http.get('api/pay/grouping', params)
}

//查询报名
export function signup(params) {
	return http.get('api/signup/list', params)
}

//查询报名
export function invoice(params) {
	return http.post('api/signup/invoice', params)
}

//员工登录
export function loginSign(params) {
	return http.post('api/signup/login', params)
}

/**报名详情 */
export function detail(params) {
	return http.get('api/signup/detail', params)
}

/**核验信息 */
export function verification(params) {
	return http.post('api/signup/verification', params)
}


//更改配置项，阻止loading提示
// export function getUserInfo(params){
// 	return http.post('api/login', {custom:{ShowLoading:false}})
// }
