//格式化金额(后面两位小数会四舍五入)
function moneyFormats(value, digit = false) {
  // console.log("before format = " + value);
  if (value == '' || value === undefined) {
    // console.log("return 0");
    return '¥0';
  }
  // let a = Number(value);  //转为数字格式
  // let b = a.toLocaleString('zh', { style: 'currency', currency: 'CNY' });
  // console.log("return format=" + b);
  // return b
  let b = String(value).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  if (!digit) {
    b = b.split('.')[0];
  }

  return '¥' + b;
}

function getLevelName(level) {
  let level1 = JSON.stringify(level)
  let levelName;
  if (level1 == '0') {
    levelName = "普通会员"
  } else if (level1 == '1') {
    levelName = "体验官"
  } else if (level1 == '2') {
    levelName = "膳粮大使"
  } else if (level1 == '3') {
    levelName = "事业合伙人"
  }
  return levelName
}

function publicDesensitization(temp) {
  //先将内置的 arguments 转换为真正的数组
  var dataArr = Array.prototype.slice.apply(arguments);
  for (var i = 0; i < dataArr.length; i++) {
    var data = dataArr[i];
    if (/^\d{16}|\d{19}$/.test(data)) {
      //银行卡号  后四位
      data = "****" + data.substr(-4);
    }
    dataArr[i] = data;
  }

  return dataArr;
}


module.exports = {
  moneyFormats: moneyFormats,
  getLevelName: getLevelName,
  publicDesensitization: publicDesensitization
}